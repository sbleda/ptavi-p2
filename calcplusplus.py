#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo
import calcoohija
import csv

if __name__ == "__main__":
    with open('fichero.csv', 'r') as archivo:
        lineas_fichero = csv.reader(archivo, skipinitialspace=False)

        for linea in archivo:
            linea = linea.split(',')
            print(linea)
            numeros = linea[1:]
            n = len(numeros)

            op1 = int(linea[1])
            op2 = int(linea[2])

            if linea[0] == "suma":
                r = calcoo.Calc.suma(calcoo.Calc.suma, op1, op2)
            elif linea[0] == "resta":
                r = calcoo.Calc.resta(calcoo.Calc.resta, op1, op2)
            elif linea[0] == "multiplicacion":
                r = calcoohija.CalcHija.mul(calcoohija.CalcHija.mul, op1, op2)
            elif linea[0] == "division":
                r = calcoohija.CalcHija.div(calcoohija.CalcHija.div, op1, op2)

            if len(linea) != 2:
                if linea[0] == "suma":
                    while(n > 2):
                        r = r + int(linea[n])
                        n = n-1
                elif linea[0] == "resta":
                    while(n > 2):
                        r = r - int(linea[n])
                        n = n-1
                elif linea[0] == "multiplicacion":
                    while(n > 2):
                        r = r * int(linea[n])
                        n = n-1
                elif linea[0] == "division":
                    while(n > 2):
                        r = r / int(linea[n])
                        n = n-1

            print(r)
