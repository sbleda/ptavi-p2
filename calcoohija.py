#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo


class CalcHija():

    def mul(self, op1, op2):

        self.op1 = op1
        self.op2 = op2
        CalcHija.mul = op1 * op2

        return(CalcHija.mul)

    def div(self, op1, op2):

        self.op1 = op1
        self.op2 = op2
        CalcHija.div = op1 / op2

        return(CalcHija.div)


if __name__ == "__main__":
    try:
        op1 = int(sys.argv[1])
        op2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")
    if sys.argv[2] == "suma":
        result = calcoo.Calc.suma(calcoo.Calc.suma, op1, op2)
    elif sys.argv[2] == "resta":
        result = calcoo.Calc.resta(calcoo.Calc.resta, op1, op2)
    elif sys.argv[2] == "multiplicacion":
        result = CalcHija.mul(CalcHija.mul, op1, op2)
    elif sys.argv[2] == "division":
        result = CalcHija.div(CalcHija.div, op1, op2)
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(result)
