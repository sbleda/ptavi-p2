#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calc():

    def suma(self, op1, op2):

        self.op1 = op1
        self.op2 = op2
        Calc.suma = op1 + op2

        return(Calc.suma)

    def resta(self, op1, op2):

        self.op1 = op1
        self.op2 = op2
        Calc.resta = op1 - op2

        return(Calc.resta)


if __name__ == "__main__":
    try:
        op1 = int(sys.argv[1])
        op2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "suma":
        result = Calc.suma(Calc.suma, op1, op2)
    elif sys.argv[2] == "resta":
        result = Calc.resta(Calc.resta, op1, op2)
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(result)
